#!/bin/bash

#This is an example script to launch cmus and the rich presence together

#Run the script in background
python ./cmus-presence.py &
#Save proccess id of the script to close it once done
last_pid=$!
#Run cmus
cmus
#Close the script once cmus is exited
kill $last_pid
