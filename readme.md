[![pypresence](https://img.shields.io/badge/using-pypresence-00bb88.svg?style=for-the-badge&logo=discord&logoWidth=20)](https://github.com/qwertyquerty/pypresence)

## cmus discord rich presence

##### Dependencies:

You should have cmus installed on your machine already.

Run `pip install -r requirements.txt` to install dependencies

##### Executing:

To run it just run `cmus-presence.py`.

To run it only while cmus is open, use `cmus-presence.sh` instead of launching cmus by itself.

This script will launch the rich presence in the background and open cmus. It will close the presence once cmus is closed.

Alternative you could add `cmus-presence.py` to launch on start up and the presence willl work every time you open cmus, tho is not recommended.
