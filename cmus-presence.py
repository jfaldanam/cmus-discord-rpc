import json
import time
import sched
import subprocess

from pypresence import Presence

#Queries cmus and returns the result
#If cmus not opened returns string as if stopped
def get_status() -> str:
    try:
        return subprocess.check_output("cmus-remote -Q", shell=True).decode('utf-8')
    except:
        return 'status closed\n'

#Get epoch in seconds
def get_time() -> int:
    return int(round(time.time()))

#For string values to be accepted by pypresence
#they must have at least 2 characters
def check_len(value :str) -> str:
    if isinstance(value, str):
        value = value if (len(value)>=2) else (value + '  ')
    return value

#Finds a substring from input that is placed between the first appearance
#of first_substring and the first appearance of last_substring
def find_between(input: str, first_substring: str, last_substring: str) -> str:
    try:
        start = input.index(first_substring) + len(first_substring)
        end = input.index(last_substring, start)
        result = check_len(input[start:end])
        return result
    except ValueError:
        return "404" #Not Found

#Parses the output of cmus-remote
#Returns: playing state, title, author, and time left for the song
def status_parser(status: str) -> tuple[str, str, int, int]:
    state = find_between(status, 'status ', '\n')
    title = find_between(status, '\ntag title ', '\n')
    author = find_between(status, '\ntag artist ', '\n')
    position = int(find_between(status, '\nposition ', '\n'))
    duration = int(find_between(status, '\nduration ', '\n'))
    song_left = duration - position
    return state, title, author, song_left

# Update status and schedules the next update
def update_status(scheduler: sched.scheduler) -> None:
    status = get_status()
    state, title, author, song_left = status_parser(status)
    time_seconds = get_time()

    if state == 'paused': #Update presence while paused
        rich_presence.update(
            details = title,
            state = 'by ' + author,
            large_image = 'cmus-rpc_png',
            large_text = 'cmus music player',
            small_image = 'paused_png',
            small_text = 'Paused'
        )
    elif state == 'stopped': #Update presence while stopped
        rich_presence.update(
            details = '404: Music Not Found',
            state = 'Stopped',
            large_image = 'cmus-rpc_png',
            large_text = 'cmus music player'
        )
    elif state == 'playing': #Update presence while playing
        rich_presence.update(
            details = title,
            state = 'by ' + author,
            large_image = 'cmus-rpc_png',
            large_text = 'cmus music player',
            small_image = 'play_png',
            small_text = 'Playing',
            start = time_seconds,
            end = time_seconds + song_left
        )
    else: #Remove presence while cmus is closed
        rich_presence.clear()

    #Schedule next status update in 5 seconds or after the current song
    next_update = 5 if (song_left > 5 or song_left <= 0) else song_left
    scheduler.enter(next_update, 0, update_status, kwargs={"scheduler": scheduler})

if __name__ == "__main__":

    #Reads the client_id from the config file
    with open('config.json') as config:
        client_id = json.load(config)['client_id']

    #Creates the rich presence and connects to the discord client
    rich_presence = Presence(client_id)
    rich_presence.connect()

    #Configure scheduler to update status
    scheduler = sched.scheduler(time.time, time.sleep)
    scheduler.enter(0, 0, update_status, kwargs={"scheduler": scheduler})

    #Start scheduler
    scheduler.run(blocking=True)
